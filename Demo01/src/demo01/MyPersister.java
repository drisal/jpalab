/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo01;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/**
 *
 * @author DeepR
 */
public class MyPersister {
    private EntityManagerFactory emf;
    private EntityManager em;

    public MyPersister() {
        emf = Persistence.createEntityManagerFactory("Demo01PU");
        em = emf.createEntityManager();
    }
    
    

//    void save(Student jack) {
//        EntityTransaction tx = em.getTransaction();
//        tx.begin();
//        em.persist(jack);
//        tx.commit();
//    }
    
    void end() {
        em.close();
        emf.close();
    }
    
}
