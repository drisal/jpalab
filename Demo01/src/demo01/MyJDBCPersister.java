/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo01;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DeepR
 */
public class MyJDBCPersister {

    private static Connection conn;
    private static Statement stmt;
    private static PreparedStatement pstmt;

    public MyJDBCPersister() {

    }

    public void getConnection() {
        try {
            if (conn != null) {
                conn = DriverManager.getConnection("jdbc:mysql://localhost:8888/ebookshop", "myuser", "xxxx");
                stmt = conn.createStatement();
                conn.setAutoCommit(false);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MyJDBCPersister.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void endConnection() {
        try {
            if (conn != null) {
                stmt.close();
                conn.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(MyJDBCPersister.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void save() throws SQLException {
        getConnection();
        try {
            pstmt = conn.prepareStatement("insert into books values (?, ?, ?, ?, ?)");
            pstmt.setInt(1, 7001);  // Set values for parameters 1 to 5
            pstmt.setString(2, "Jack");            
            pstmt.setDouble(4, 88.88);
            pstmt.setInt(5, 88);
            int rowsInserted = pstmt.executeUpdate();
            conn.commit();   
            System.out.println(rowsInserted + "rows affected.");
            pstmt.close();
            endConnection();
        } catch (SQLException ex) {
            Logger.getLogger(MyJDBCPersister.class.getName()).log(Level.SEVERE, null, ex);
            conn.rollback();
            endConnection();
        }
    }

}
